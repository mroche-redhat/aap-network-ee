# Ansible Automation Platform Networking Demo

This is a quick guide on creating a custom execution environment for managing network devices on AAP 2.x.

Table of Contents

* [Prereqs](#prereqs)
* [Guide](#guide)
  * [Specifying Dependencies](#specifying-dependencies)
  * [Configuring the Execution Environment](#configuring-the-execution-environment)
  * [Building the Execution Environment](#building-the-execution-environment)
  * [Adding the environment to AAP](#adding-the-environment-to-aap)

## Prereqs

**Required**

* Ansible Builder (either from PyPI or the AAP yum repository)
* A container runtime (podman)
* A container registry to push to
* Ansible Automation Platform 2.x

**Recommended**

* An entitled RHEL system to build the images on (subscription-manager can be used on Fedora and CentOS as well)
* A Red Hat Registry service account

## Guide

In this repo you will find four documents.

* execution-environment.yml
* galaxy.yml
* python.txt
* system.txt

### Specifying Dependencies

A custom execution environment (EE) is needed when the default environments don't provide the necessary collections and their dependencies your projects require. There are three types of dependencies handled by EEs: collections, Python libraries, and host packages.

The dependency files can be named however you want, but they need to follow the syntax the build system uses. Ansible Galaxy dependencies are specified in the standard [requirements.yml](https://docs.ansible.com/ansible/devel/user_guide/collections_using.html#install-multiple-collections-with-a-requirements-file) format, Python in [Pip compatible](https://pip.pypa.io/en/stable/reference/requirements-file-format/) format, and system packages in the [bindep](https://docs.opendev.org/opendev/bindep/latest/readme.html) format.

Depending on the collections you are using, specifying Python and system dependencies manually may not be necessary. For example, the [`cisco.ios`](https://github.com/ansible-collections/cisco.ios) collection provides its own `requirements.txt` and `bindep.txt` files that the `ansible-builder` tool will use in resolving the necessary dependencies. If a collection doesn't provide these file itself, or you need other dependencies outside the scope of the collections you will need to specify them yourself.

In this repository all of the collections follow best practice and describe the dependencies they need, so the inclusion of `python.txt` and `system.txt` is purely academic.

### Configuring the Execution Environment

With the dependencies set, the execution environment needs to be configured. The two main sections are the `build_arg_defaults` and `dependencies` sections.

For the build arguments, its recommended to specify the Base (runtime) and Builder (creation) images to use when generating your execution environment. As we are deploying on AAP we will use the Red Hat supported AAP 2.1 base and builder images that use Ansible 2.12. These images are based on UBI 8 and as such can be used without a Red Hat subscription. However, it is possible that a host package dependency may not be included in the UBI repositories, hence the recommendation to run the builder on an entitled system. This is the case for this project, which will try to pull in `libssh-devel` which is only avaialble in the full RHEL repositories.

Alternatively, images based on CentOS Stream 8 can be used but are unsupported by Red Hat: [Base Image](https://quay.io/repository/ansible/ansible-runner?tab=tags), [Builder Image](https://quay.io/repository/ansible/ansible-builder?tab=tags)

```yaml
build_arg_defaults:
  EE_BASE_IMAGE: "registry.redhat.io/ansible-automation-platform-21/ee-minimal-rhel8:latest"
  EE_BUILDER_IMAGE: "registry.redhat.io/ansible-automation-platform-21/ansible-builder-rhel8:latest"
```

In order to use the Red Hat images, authenticate to the Red Hat registry using a [service account](https://access.redhat.com/terms-based-registry/), such as the one created for AAP.

```bash
$ podman login registry.redhat.io
Username: <ServiceAccountName>
Password: <ServiceAccountPassword>
```

The dependencies section is straightfoward, just mapping which files to use for the given dependency type:

```yaml
dependencies:
  galaxy: galaxy.yml
  python: python.txt
  system: system.txt
```

There are other sections that are available for use but aren't needed in the context of this demo. They can be reviewed in the [Execution Environment Setup Reference](https://docs.ansible.com/automation-controller/latest/html/userguide/ee_reference.html).

### Building the Execution Environment

With the base configuration done, all that's left to do is build the EE image. As I'm using the registry repository of `quay.io/omenos/aap-ee`, I'll specify the custom tag name to use when building and maximize the verbosity of the output:

```bash
$ ansible-builder build -v 3 --tag quay.io/omenos/aap-ee:21-networking-ee
... build output ...

$ podman images quay.io/omenos/aap-ee:21-networking-ee
REPOSITORY             TAG               IMAGE ID      CREATED         SIZE
quay.io/omenos/aap-ee  21-networking-ee  5157588846f5  10 seconds ago  482 MB
```

### Adding the environment to AAP

With the image built, we can now push the image to the registry.

```bash
$ podman push quay.io/omenos/aap-ee:21-networking-ee
...pushing to registry...
```

Now in the AAP web view, navigate to Administration > Execution Environments and click "Add". Fill in the options with the appropriate details and hit "Save". Some notes:

* If your image is stored in a private registry, create a "Container Registry" credential and specify it here. Othewise AAP will not be able to pull the image.
* Choose the appropriate pull method for your image. If you plan on manually updating the image with a different tag (e.g. `-v1`, `-v2`, etc) then it may be best to choose "Only pull the image if not present before running". If you plan on using transient tags (`:latest`), it may be best to always pull the image before a run to ensure you have the desired image.

The EE will now be available for use when creating/modifying job templates and projects. The EE can also be defined as the default runtime for an entire project so all job templates will inherit that preference.